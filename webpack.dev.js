const { merge } = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: "development",
    entry: path.resolve(__dirname, "src/index.js"),
    output: {
        filename: "devbundle.js",
        path: path.resolve(__dirname, "devdist")
    }
});
