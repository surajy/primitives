import React, { useState } from 'react';
import { Button, Anything, Modal, Glider, Carousel, List } from './components/index.js';

function App(){

    const page = {
	type: "element",
        layout: 1,
	data: {
            items: [
                {
                    type: "element",
                    data: {
                        items: [
                            {
		                type: "card",
		                layout: 1/4,
		                heading: "Card1",
		                subsubheading: "Subheading",
		                image: ["https://picsum.photos/150", "https://picsum.photos/160"],
                                copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
	                    },
	                    {
		                type: "card",
		                layout: 1/4,
		                heading: "Card2",
		                subsubheading: "Subheading",
		                image: ["https://picsum.photos/150", "https://picsum.photos/160"],
                                copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
	                    },
                            {
		                type: "card",
		                layout: 1/4,
		                heading: "Card3",
		                subsubheading: "Subheading",
		                image: ["https://picsum.photos/150", "https://picsum.photos/160"],
                                copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                            },
                            {
		                type: "card",
		                layout: 1/4,
		                heading: "Card4",
		                subsubheading: "Subheading",
		                image: ["https://picsum.photos/150", "https://picsum.photos/160"],
                                copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
	                    }
                        ],
                    }
                },
                {
                    layout: 1/2,
                    type: "accordion",
                    data: {
                        name: "Accordion bar",
                        content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                    }
                },
                {
                    type: "buttongroup",
                    layout: 1/2,
                    children: [
                        {
                            name: "button1"
                        },
                        {
                            name: "button2"
                        },
                        {
                            name: "button3"
                        }
                    ]
                }
	    ]
        }
    };

    const data = {
        type: "card",
        fetch: {
            url: "https://jsonplaceholder.typicode.com/todos",
            mapping: {
                userId: "heading",
                id: "subsubheading"
            }
        }
    };
    
    function randrange(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function randImages(n){
        return [...Array(n).keys()]
            .map(index => {
                const size = randrange(100, 150);
                return `https://picsum.photos/${size}`;
            });
    }

    const [open, setopen] = useState(false);
    
    return (
        <div className="mb5">
          <Anything {...page}/>
        </div>
    );
}

export default App;
