import React from 'react';
import ReactDom from 'react-dom';
import App from './App.js';
import "tachyons/css/tachyons.css";
import "./index.css";

ReactDom.render(<App/>, document.getElementById('root'));
