import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Glider from 'glider-js';
import { Anything } from '../index.js';
import "glider-js/glider.css";

function GliderBase({items, ...props}){

    useEffect(() => {
        let gliderElem = document.querySelector('.glider');
        let glider = new Glider(gliderElem, {
            slidesToShow: 3,
            slidesToScroll: 'auto',
            draggable: true
        });
    });
    
    return (
        <div className="glider">
          {
              items.map((item, index) => <Anything {...item} key={index}/>)
          }
        </div>
    );
}

export default GliderBase;
