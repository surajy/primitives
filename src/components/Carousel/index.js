import React, { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { Image } from '../index.js';
import styled from 'styled-components';
import Siema from 'siema';
import clsx from 'clsx';
import PropTypes from 'prop-types';

function CarouselElement({image}){
    return (
        <CarouselElementBase>
          <Image image={image} className="h-100 w-100"/>
        </CarouselElementBase>
    );
}

function Carousel({items, ...props}){

    function rand(){
        return uuidv4().replace(/[-]/gi, "");
    }

    const random = rand();
    
    useEffect(() => {
        new Siema({
            selector: `.siema${random}`,
            perPage: props.responsive ? {
                768: 2,
                1024: 3,
            } : 1,
        });
    }, []);
    
    return (
        <div className={`siema${random}`}>
          {
              items.map((item, index) => <CarouselElement image={item} key={index}/>)
          }
        </div>
    );
}

const CarouselElementBase = styled('div').attrs(props => ({
    className: "tc"
}))``;

export default Carousel;
