import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Half({children}){
    return (
	<HalfBase>
	    {children}
	</HalfBase>
    );
}
export default Half;

const HalfBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-100-m w-50-l", 
               props.all && "w-20")
}))``;