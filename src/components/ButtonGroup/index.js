import React from 'react';
import Button from '../Button';
import styled from 'styled-components';
import PropTypes from 'prop-types';

function ButtonGroup({children}){
    return (
        <ButtonGroupBase>
          {
              children.map((child, index) =>
                           <Button {...child} key={index}>{child.name}</Button>
                          )
          }
        </ButtonGroupBase>
    );
}

export default ButtonGroup;

const ButtonGroupBase = styled('div').attrs(props => ({
    className: "flex flex-row flex-wrap"
}))``;

ButtonGroup.propTypes = {
    children: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired
    })).isRequired
};

ButtonGroup.defaultProps = {
    children: []
};
