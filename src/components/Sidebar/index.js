import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';

function Sidebar({items, open}){
    return (
        <SidebarBase>
          {
              items.map((item, index) => <SidebarElement href={item.path} key={index}>
                                           {item.name}
                                         </SidebarElement>)
          }
        </SidebarBase>
    );
}

export default Sidebar;

const SidebarBase = styled('nav').attrs(props => ({
    className: clsx("z-max bg-red white w-25-l w-50-m w-75-l absolute top-0 left-0")
}))`
height: 100vh;
visibility: ${props => props.open ? "visible" : "hidden"};
opacity: ${props => props.open ? "1" : "0"};
transform: ${props => props.open ? "translateX(0)" : "translateX(-50%)"};
transition: all 0.2s;
`;

const SidebarElement = styled('a').attrs(props => ({
    className: "bb link yellow"
}))``;

