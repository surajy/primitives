import React from 'react';
import styled from 'styled-components';
import Copy from '../Copy';
import PropTypes from 'prop-types';

function Collapse({content, open}){
    return (
        <CollapseBase open={open}>
          <InnerCollapse>
            <Copy content={content}/>
          </InnerCollapse>
        </CollapseBase>
    );
}

export default Collapse;

const CollapseBase = styled('div').attrs(props => ({
    className: "br--bottom br2 b--moon-gray"
}))`
max-height: ${props => props.open ? "1000px" : "0px" };
border: ${props => props.open ? "1px" : "none" };
border-top: none;
overflow: hidden;
transition: max-height 0.3s ease;
`;

const InnerCollapse = styled('div').attrs(props => ({
    className: "pa2"
}))``;

Collapse.propTypes = {
    content: PropTypes.string,
    open: PropTypes.bool
};

Collapse.defaultProps = {
    content: "",
    open: false
};
