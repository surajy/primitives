import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';

function Leaf({name, path}){
    return (
        <LeafBase href={path}>
          {name}
        </LeafBase>
    );
}

function Node({name, items}){
    return (
        <NodeBase>
        </NodeBase>
    );
}
 
function Navitem({items}){
    // [{name: "", path: ""} => leaf, {name: "", items: [], node: bool} => node]
    return (
        <>
          {
              items.map((item, index) => {
                  if (item.node)
                      return <Node />;
                  else
                      return 2;
              })
          }
        </>
    );
}

function Navbar({items}){
}

export default Navbar;

const NavbarBase = styled('nav').attrs(props => ({
    className: ""
}))``;

const NavitemBase = styled('div').attrs(props => ({
    className: ""
}))``;

const NodeBase = styled('div').attrs(props => ({
    className: ""
}))``;

const LeafBase = styled('a').attrs(props => ({
    className: ""
}))``;
