import React, { useState } from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
import PropTypes from 'prop-types';

function Copy({content}){

    const head = content.slice(0, 100);
    const rest = content.length > 100 ? content.slice(100): "";
    
    const [more, setMore] = useState(false);
    return (
        <>
          <CopyBase>
            {head}{more && rest}
            { rest && <ReadMore more={more} onClick={() => setMore(!more)}/>}
          </CopyBase>
        </>);
}

export default Copy;

const CopyBase = styled('p').attrs(props => ({
    className: clsx("lh-copy f4 pv2", {[props.measure]: Boolean(props.measure),
                                       "" : Boolean(!props.measure)})
}))`
margin: 0;
`;

const ReadMore = styled('button').attrs(props => ({
    children: props.more ? "Read Less" : "Read More",
    className: "dib pointer bn bg-white blue outline-0"
}))``;

Copy.propTypes = {
    content: PropTypes.string
};

Copy.defaultProps = {
    content: ""
};
