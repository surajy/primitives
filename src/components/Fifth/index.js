import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Fifth({children}){
    return (
	<FifthBase>
	    {children}
	</FifthBase>
    );
}
export default Fifth;

const FifthBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-50-m w-20-l", 
               props.all && "w-20")
}))``;