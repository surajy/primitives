import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

function Heading({name}){
    return (
        <HeadingBase>
            {name}
        </HeadingBase>
    );
}

export default Heading;

const HeadingBase = styled('h3').attrs(props => ({
    className: "f2 lh-solid helvetica ttc mv1 near-black"
}))``;

Heading.propTypes = {
    name: PropTypes.string
};

Heading.defaultProps = {
    name: ""
};
