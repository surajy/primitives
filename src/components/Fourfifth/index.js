import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Fourfifth({children}){
    return (
	<FourfifthBase>
	    {children}
	</FourfifthBase>
    );
}
export default Fourfifth;

const FourfifthBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-100-m w-80-l", 
               props.all && "w-20")
}))``;