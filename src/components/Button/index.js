import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import clsx from 'clsx';

function Button({name, ...props}){
    return (
        <ButtonBase {...props}>
          {name}
        </ButtonBase>
    );
}

export default Button;

const ButtonBase = styled('button').attrs(props => ({
    className: clsx(`pointer pv1 ph3 ba br1 ttu b helvetica ba b--gray shadow-5 f4 outline-0 bg-animate`,
                    props.color ? `${props.color}` : "dark-gray",
                    props.bg ? `bg-${props.bg}` : "bg-light-gray",
                    props.full && "w-100")
}))``;

Button.propTypes = {
    name: PropTypes.any.isRequired
};

Button.defaultProps = {
    name: ""
};

