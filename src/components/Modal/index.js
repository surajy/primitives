import React from 'react';
import styled from 'styled-components';
import { Anything } from '../index.js';
import clsx from 'clsx';

function Modal({open, title, content, onClose}){
    if (open)
        return (
            <Backdrop>
              <ModalBase>
                <ModalBar>
                  {title}
                  <Close onClick={() => onClose()}>Close</Close>
                </ModalBar>
                <ModalContent>
                  <Anything {...content}/>
                </ModalContent>
              </ModalBase>
            </Backdrop>
        );
    else
        return <></>;
}

export default Modal;

const ModalBar = styled('div').attrs(props => ({
    className: "f3 ttu tl helvetica bg-light-gray pa2 flex flex-row justify-between items-center pl3"
}))``;

const Close = styled('button').attrs(props => ({
    className: "br2 bn ph2 pv1 pointer bg-animate hover-bg-moon-gray"
}))``;

const ModalContent = styled('div').attrs(props => ({
    className: "pa2 overflow-auto bg-white center"
}))``;

const ModalBase = styled('div').attrs(props => ({
    className: clsx(`br2 ba b--moon-gray bg-white shadow-5 ma2 overflow-auto mh5-l mh2 z-max
 top-0 bottom-0 left-0 right-0 absolute top-1-l bottom-2-l`)
}))``;

const Backdrop = styled('div').attrs(props => ({
    className: "fixed top-0 right-0 left-0 bottom-0"
}))`
background: rgba(0, 0, 0, 0.4);
`;
