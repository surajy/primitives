import React, { useState } from 'react';
import styled from 'styled-components';
import { Content, Collapse, Header, Carousel, Copy, Button } from '../index.js';
import clsx from 'clsx';
import PropTypes from 'prop-types';

function Card({heading, subheading, subsubheading, copy, image, actions, rest, collapse}){
    
    const [open, setOpen] = useState(false);
    
    return (
        <CardBase>
          {
              (heading || subheading || subsubheading || copy || image || rest) &&
                  <Content heading={heading} subheading={subheading}
                           subsubheading={subsubheading} image={image} copy={copy} rest={rest}/>
          }
          {
              actions &&
                  <Bottom>
                    <ActionArea>
                      {
                          actions.map((action, index) => <Action {...action} key={index} full>
                                                           {action.name}
                                                         </Action>)
                      }
                    </ActionArea>
                    {
                        collapse && 
                            <Expand open={open} onClick={() => setOpen(!open)} />
                    }
                  </Bottom>
          }
          {
              collapse &&
                  <Collapse content={collapse} open={open}/>
          }
        </CardBase>
    );
}

const CardBase = styled('div').attrs(props => ({
    className: "pa3 br2 shadow-5 flex flex-column ba b--moon-gray w-100 pb1"
}))``;

const ActionArea = styled('div').attrs(props => ({
    className: "flex flex-row mv2 justify-start flex-wrap"
}))``;

const Action = styled('button').attrs(props => ({
    className: clsx("f4 ttu br2 bg-moon-gray dark-gray pv2 ph3 grow fw1 tc mh1 ba bw1 b--blue b helvetica",
                    !props.primary && "bg-white blue",
                    props.primary && "bg-blue white"),
    name: props.children
}))``;

const Images = styled('div').attrs(props => ({
    className: "w-100"
}))``;

const Expand = styled('button').attrs(props => ({
    className: `pa1 bg-white bn outline-0 pointer blue f4`,
    children: props.open ? "Less" : "More"
}))``;

const Bottom = styled('div').attrs(props => ({
    className: "flex flex-column"
}))``;

export default Card;

Card.propTypes = {
    heading: PropTypes.string,
    subheading: PropTypes.string,
    subsubheading: PropTypes.string,
    copy: PropTypes.string,
    image: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    actions: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        handler: PropTypes.func.isRequired
    })),
    rest: PropTypes.any,
    collapse: PropTypes.any
};

Card.defaultProps = {
    heading: "",
    subheading: "",
    subsubheading: "",
    copy: "",
    image: "",
    actions: [],
    rest: null,
    collapse: null
};
