import React from 'react';

import Accordion from '../Accordion';
import Anything from '../Anything';
import Button from '../Button';
import ButtonGroup from '../ButtonGroup';
import Card from '../Card';
import Carousel from '../Carousel';
import Collapse from '../Collapse';
import Content from '../Content';
import Copy from '../Copy';
import Element from '../Element';
import Fifth from '../Fifth';
import Fourfifth from '../Fourfifth';
import Fourth from '../Fourth';
import Glider from '../Glider';
import Half from '../Half';
import Header from '../Header';
import Heading from '../Heading';
import Image from '../Image';
import Layout from '../Layout';
import List from '../List';
import Modal from '../Modal';
import Navbar from '../Navbar';
import Select from '../Select';
import Sidebar from '../Sidebar';
import Single from '../Single';
import Subheading from '../Subheading';
import Subsubheading from '../Subsubheading';
import Tab from '../Tab';
import Third from '../Third';
import Threefifth from '../Threefifth';
import Threefourth from '../Threefourth';
import Twofifth from '../Twofifth';
import Twothird from '../Twothird';;

function Component({type, subtype, data, extra}){
const allProps = {
        type: subtype,
        ...data,
        ...extra
    };

switch(type){
case "accordion":
return (<Accordion {...allProps}/>);
case "anything":
return (<Anything {...allProps}/>);
case "button":
return (<Button {...allProps}/>);
case "buttongroup":
return (<ButtonGroup {...allProps}/>);
case "card":
return (<Card {...allProps}/>);
case "carousel":
return (<Carousel {...allProps}/>);
case "collapse":
return (<Collapse {...allProps}/>);
case "content":
return (<Content {...allProps}/>);
case "copy":
return (<Copy {...allProps}/>);
case "element":
return (<Element {...allProps}/>);
case "fifth":
return (<Fifth {...allProps}/>);
case "fourfifth":
return (<Fourfifth {...allProps}/>);
case "fourth":
return (<Fourth {...allProps}/>);
case "glider":
return (<Glider {...allProps}/>);
case "half":
return (<Half {...allProps}/>);
case "header":
return (<Header {...allProps}/>);
case "heading":
return (<Heading {...allProps}/>);
case "image":
return (<Image {...allProps}/>);
case "layout":
return (<Layout {...allProps}/>);
case "list":
return (<List {...allProps}/>);
case "modal":
return (<Modal {...allProps}/>);
case "navbar":
return (<Navbar {...allProps}/>);
case "select":
return (<Select {...allProps}/>);
case "sidebar":
return (<Sidebar {...allProps}/>);
case "single":
return (<Single {...allProps}/>);
case "subheading":
return (<Subheading {...allProps}/>);
case "subsubheading":
return (<Subsubheading {...allProps}/>);
case "tab":
return (<Tab {...allProps}/>);
case "third":
return (<Third {...allProps}/>);
case "threefifth":
return (<Threefifth {...allProps}/>);
case "threefourth":
return (<Threefourth {...allProps}/>);
case "twofifth":
return (<Twofifth {...allProps}/>);
case "twothird":
return (<Twothird {...allProps}/>);
default:
return null;
}
}

export default function({type, subtype, data, extra}){
    const size = data?.layout || extra?.layout || 1;
    return (
        <Layout size={size}>
          <Component type={type} subtype={subtype} data={data} extra={extra}/>
        </Layout>
    );
}
