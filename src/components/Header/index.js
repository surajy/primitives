import React from 'react';
import Heading from '../Heading';
import Subheading from '../Subheading';
import Subsubheading from '../Subsubheading';
import styled from 'styled-components';
import PropTypes from 'prop-types';

function Header({heading, subheading, subsubheading}){
    return (
	<HeaderBase>
	    <Heading name={heading}/>
	    <Subheading name={subheading} />
	    <Subsubheading name={subsubheading} />
	</HeaderBase>
    );
}

export default Header;

Header.propTypes = {
    heading: PropTypes.string,
    subheading: PropTypes.string,
    subsubheading: PropTypes.string
};

Heading.defaultProps = {
    heading: "",
    subheading: "",
    subsubheading: ""
};

const HeaderBase = styled('div').attrs(props => ({
    className: "flex flex-column"
}))``;
