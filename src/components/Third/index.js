import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Third({children}){
    return (
	<ThirdBase>
	    {children}
	</ThirdBase>
    );
}
export default Third;

const ThirdBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-100-m w-third-l", 
               props.all && "w-20")
}))``;