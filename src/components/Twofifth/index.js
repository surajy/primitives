import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Twofifth({children}){
    return (
	<TwofifthBase>
	    {children}
	</TwofifthBase>
    );
}
export default Twofifth;

const TwofifthBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-50-m w-40-l", 
               props.all && "w-20")
}))``;