import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Threefifth({children}){
    return (
	<ThreefifthBase>
	    {children}
	</ThreefifthBase>
    );
}
export default Threefifth;

const ThreefifthBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-50-m w-60-l", 
               props.all && "w-20")
}))``;