import React from 'react';
import styled from 'styled-components';
import Anything from '../Anything';
import Header from '../Header';
import Carousel from '../Carousel';
import Copy from '../Copy';
import PropTypes from 'prop-types';
import "glider-js/glider.css";

function Content({heading, subheading, subsubheading, image, copy, rest}){
    return (
        <ContentBase>
          {
              (heading || subheading || subsubheading) &&
                  <Header heading={heading} subheading={subheading} subsubheading={subsubheading}/>
          }
          {
              image &&
                  <Images>
                    {
                        Array.isArray(image) ?
                            <Carousel items={image}/>
                        :
                        <Image image={image} rounded/>
                    }
                  </Images>
          }
          {
              copy &&
                  <Copy content={copy}/>
          }
          {
              rest &&
                  <Rest>
                    <Anything {...rest}/>
                  </Rest>
          }
        </ContentBase>
    );
}

export default Content;

Content.propTypes = {
    heading: PropTypes.string,
    subheading: PropTypes.string,
    subsubheading: PropTypes.string,
    image: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string)
    ]),
    copy: PropTypes.string,
    rest: PropTypes.any
};

Content.defaultProps = {
    heading: "",
    subheading: "",
    subsubheading: "",
    image: "",
    copy: "",
    rest: null
};

const ContentBase = styled('div').attrs(props => ({
    className: ""
}))``;

const Images = styled('div').attrs(props => ({
    className: "w-100 pa2"
}))``;

const Rest = styled('div').attrs(props => ({
    className: "mv2"
}))``;
