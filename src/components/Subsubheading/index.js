import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

function Subsubheading({name}){
    return (
        <SubsubheadingBase>
            {name}
        </SubsubheadingBase>
    );
}

export default Subsubheading;

const SubsubheadingBase = styled('h5').attrs(props => ({
    className: "f5 helvetica ttc ttu mv1 fw1 mid-gray lh-solid"
}))``;

Subsubheading.propTypes = {
    name: PropTypes.string
};

Subsubheading.defaultProps = {
    name: ""
};
