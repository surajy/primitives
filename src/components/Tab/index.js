import React, { useState } from 'react';
import styled from 'styled-components';
import clsx from 'clsx';

function Tab({items}){

    const [current, setCurrent] = useState(0);
    
    return (
        <TabContainer>
          <TabHeaders>
            {
                items.map((item, index) =>
                          <TabButton active={index === current}
                                     key={index}
                                     onClick={() => setCurrent(index)}>
                            {item.name}
                          </TabButton>)
            }
          </TabHeaders>
          <TabContent>
            {items[current].content}
          </TabContent>
        </TabContainer>
    );
}

export default Tab;

const TabHeaders = styled('div').attrs(props => ({
    className: "flex flex-row justify-around"
}))``;

const TabButton = styled('button').attrs(props => ({
    className: clsx("w-100", props.active && "bg-blue near-white", "bn ph3 pv2 ttu helvetica pointer")
}))``;

const TabContent = styled('div').attrs(props => ({
    className: "pa2"
}))``;

const TabContainer = styled('div').attrs(props => ({
    className: "flex flex-column ba w-100 br2 shadow-5 b--moon-gray"
}))``;
