import React, { useState } from 'react';
import styled from 'styled-components';
import Button from '../Button';
import Collapse from '../Collapse';
import clsx from 'clsx';

function Accordion({name, content}){

    const [open, setOpen] = useState(false);
    
    return(
        <AccordionBase open={open}>
          <AccordionBar onClick={() => setOpen(!open)} open={open}>
            {name}
            <Expand open={open} onClick={() => setOpen(!open)}>
              &#x025C2;
            </Expand>
          </AccordionBar>
          <Collapse content={content} open={open}>
          </Collapse>
        </AccordionBase>
    );
}

export default Accordion;

const AccordionBase = styled('div').attrs(props => ({
    className: clsx("br2 shadow-5", props.open && "ba b--moon-gray", !props.open && "bn")
}))``;

const AccordionBar = styled(Button).attrs(props => ({
    className: clsx("w-100 bn pv2 mid-gray tl fw1 bg-animate br0 flex flex-row justify-between",
                    props.open && "br--top br2"),
    name: props.children
}))`
box-shadow: ${props => props.open ? "initial" : "none"};
`;

const Expand = styled('div').attrs(props => ({
    className: `br-pill flex flex column justify-center items-center bn bg-light-gray mid-gray
    outline-0 bg-animate`
}))`
transform: ${props => props.open ? "rotate(90deg)" : "rotate(-90deg)"};
transition: all 0.2s;
`;
