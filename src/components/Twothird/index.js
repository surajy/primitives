import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Twothird({children}){
    return (
	<TwothirdBase>
	    {children}
	</TwothirdBase>
    );
}
export default Twothird;

const TwothirdBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-100-m w-two-thirds-l", 
               props.all && "w-20")
}))``;