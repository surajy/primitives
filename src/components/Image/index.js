import React from "react";
import styled, { keyframes } from "styled-components";
import PropTypes from "prop-types";
import LazyLoad from "react-lazyload";
import clsx from 'clsx';

const StyledImage = styled.img.attrs(props => ({
    className: clsx("w-100", props.circle && "br-100",
                    props.rounded && "br3", props.thumbnail && "ba br1 pa1 b--moon-gray")
}))``;

const LazyImage = ({ image, alt, ...props}) => {

    return (
          <LazyLoad>
	    <StyledImage
	      src={image}
	      alt={alt}
	      {...props}
	    />
          </LazyLoad>
    );
};

LazyImage.propTypes = {
    image: PropTypes.string.isRequired,
    alt: PropTypes.string
};

export default LazyImage;
