import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Threefourth({children}){
    return (
	<ThreefourthBase>
	    {children}
	</ThreefourthBase>
    );
}
export default Threefourth;

const ThreefourthBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-50-m w-75-l", 
               props.all && "w-20")
}))``;