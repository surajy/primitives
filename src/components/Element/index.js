import React from 'react';
import styled from 'styled-components';
import { Anything } from '../index.js';
import PropTypes from 'prop-types';

function Element({items, ...props}){

    return (
	<>
	  {
	      items.map((item, index) => <Anything {...item} key={index}/>)
	  }
	</>
    );
}

export default Element;

const ElementBase = styled('div').attrs(props => ({
    className: "flex flex-row items-stretch"
}))`
margin: 0;
padding: 0;
`;

Element.propTypes = {
    children: PropTypes.arrayOf(PropTypes.any)
};

Element.defaultProps = {
    children: []
};
