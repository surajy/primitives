import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

function Subheading({name}){
    return (
        <SubheadingBase>
            {name}
        </SubheadingBase>
    );
}

export default Subheading;

const SubheadingBase = styled('h4').attrs(props => ({
    className: "f3 helvetica ttc tl lh-title mv1 fw2 dark-gray"
}))``;

Subheading.propTypes = {
    name: PropTypes.string
};

Subheading.defaultProps = {
    name: ""
};
