import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
import { Anything } from '../index.js';

function ListItem({item, ...props}){
    return (
        <ListItemBase {...props}>
          {
              typeof item === 'string' && <PrimaryItem>item</PrimaryItem>
          }
          {
              typeof item === 'object' && <Anything {...item}/>
          }
        </ListItemBase>
    );
}

function List({items, ...props}){

    const lastIndex = items.length - 1;
    
    return (
        <ListBase {...props}>
          {
              items.map((item, index) => <ListItem key={index} item={item}
                                                   border={index !== lastIndex && props.border}/>)
          }
        </ListBase>
    );
}

export default List;

const ListBase = styled('ul').attrs(props => ({
    className: clsx(props.border && "ba br2 pv2 ph3 w-100 b--moon-gray shadow-5")
}))`
margin: 0px;
&:last-child {
border-bottom: none !important;
}
`;

const ListItemBase = styled('li').attrs(props => ({
    className: clsx("flex flex-column mb2", props.border && "bb b--moon-gray")
}))``;

const PrimaryItem = styled('span').attrs(props => ({
    className: "mid-gray f3 helvetica"
}))``;

const SecondaryItem = styled('span').attrs(props => ({
    className: "silver f4 roboto"
}))``;

