import Single from '../Single';
import Half from '../Half';
import Third from '../Third';
import Fourth from '../Fourth';
import Fifth from '../Fifth';
import Twothird from '../Twothird';
import Threefourth from '../Threefourth';
import Twofifth from '../Twofifth';
import Threefifth from '../Threefifth';
import Fourfifth from '../Fourfifth';
import React from 'react';

function Layout({size, children}){
switch(size){
case 1:
return <Single>{children}</Single>;
case 0.5:
return <Half>{children}</Half>;
case 0.3333333333333333:
return <Third>{children}</Third>;
case 0.25:
return <Fourth>{children}</Fourth>;
case 0.2:
return <Fifth>{children}</Fifth>;
case 0.6666666666666666:
return <Twothird>{children}</Twothird>;
case 0.75:
return <Threefourth>{children}</Threefourth>;
case 0.4:
return <Twofifth>{children}</Twofifth>;
case 0.6:
return <Threefifth>{children}</Threefifth>;
case 0.8:
return <Fourfifth>{children}</Fourfifth>;
default:
return Single;
}
}

export default Layout;