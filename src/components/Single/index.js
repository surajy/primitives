import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Single({children}){
    return (
	<SingleBase>
	    {children}
	</SingleBase>
    );
}
export default Single;

const SingleBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-100-m w-100-l", 
               props.all && "w-20")
}))``;