import React from 'react';
import styled from 'styled-components';

function Select({options}){
    return (
        <SelectBase>
            {
		options.map((option, index) =>
		    <option key={index}
			    value={option.value}>
			{option.name}
		    </option> )
            }
        </SelectBase>
    );
};

export default Select;

const SelectBase = styled('select').attrs(props => ({
    className: "nowrap outline-0 ba b--gray br1 pv1 ph3 f4 overflow-hidden pointer"
}))`
    -webkit-appearance: button;
    -moz-appearance: button;
    -webkit-user-select: none;
    -webkit-padding-end: 20px;
    -moz-padding-end: 20px;
    -webkit-padding-start: 2px;
    -moz-padding-start: 2px;
    background-position: center right;
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
    text-overflow: ellipsis;
`;
