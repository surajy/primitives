import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
function Fourth({children}){
    return (
	<FourthBase>
	    {children}
	</FourthBase>
    );
}
export default Fourth;

const FourthBase = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-100 w-50-m w-25-l", 
               props.all && "w-20")
}))``;