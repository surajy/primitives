import React from 'react';
import Component from '../Component';

function Anything({type, subtype, data, count, ...props}){
    if (count && count > 1){
        return (<>
            {
                data.map((singleData, index) => {
                    return <Component type={type} subtype={subtype}
                                      data={singleData} extra={{...props, key:index }}/>;
                })
            }
        </>);
    } else 
        return <Component type={type} subtype={subtype} data={data} extra={props}/>;
}

export default Anything;
