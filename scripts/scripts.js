/*
  RUNS ALL SCRIPTS IN ORDER
*/

const path = require('path');
const fs = require('fs');
const { exec } = require('child_process');

const allScripts = fs.readdirSync(__dirname, (err, scriptNames) => scriptNames);
const prefix = path.relative(__dirname, path.resolve(__dirname, ".."));
// remove itself from allScripts
const scripts = allScripts.filter(x => x !== "scripts.js");

// order to run the scripts
const order = ["layout", "layouts", "components", "import"];

const orderedScripts = scripts.sort((a, b) => {
    aindex = order.indexOf(a.slice(0, -3));
    bindex = order.indexOf(b.slice(0, -3));
    return aindex - bindex;
});

orderedScripts.map(script => {
    const scriptsPath = path.resolve(__dirname);
    const scriptPath = path.resolve(scriptsPath, script);
    
    exec(`node ${scriptPath}`, (error, stdout, stderr) => {
        if (error)
            console.log("error :",error);
        else if (stderr)
            console.log("stderr :", stderr);
    });
});
