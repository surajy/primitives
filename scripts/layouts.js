// GENERATES THE LAYOUT COMPONENTS
/*
  [[name, small, medium, large], ...]
  name => name of component,
  small, medium, large => fraction of width to occupy on small, medium and large devices
  postfix for tachyons classes are ns, m & l 
  ns = not small(phones),
  m = medium(tabs)
  l = large(desktops). Refer tachyons docs
  Can be converted to other css library by chaning sizetoPostfixMappings
*/

const fs = require('fs');
const path = require('path');

const layouts = [["single", 1, 1, 1],
		 ["half", 1, 1, 1/2],
		 ["third", 1, 1, 1/3],
		 ["fourth", 1, 1/2, 1/4],
		 ["fifth", 1, 1/2, 1/5],
		 ["twothird", 1, 1, 2/3],
		 ["threefourth", 1, 1/2, 3/4],
		 ["twofifth", 1, 1/2, 2/5],
		 ["threefifth", 1, 1/2, 3/5],
		 ["fourfifth", 1, 1, 4/5]];

// returns the postfix from tachyons css classes.
// eg, 1 size = 100% width, class for 100% width = w-100
// eg, 1/3 = 33.33% width, class = w-third
const sizeToPostfixMappings = {
    "1": "100",
    "1/2": "50",
    "1/3": "third",
    "1/4": "25",
    "1/5": "20",
    "2/3": "two-thirds",
    "3/4": "75",
    "2/5": "40",
    "3/5": "60",
    "4/5": "80"
};

// gets the mapping val from sizeToPostfixMappings
const mapSize = size => Object
      .entries(sizeToPostfixMappings)
      .map(([k, v]) => [eval(k), v])
      .filter(([k, v]) => k === size)[0][1];

const capitalize = x => x.charAt(0).toUpperCase().concat(x.slice(1));

// tuples => objects
// sm, md & lg are for readability
const objLayouts = layouts.map(([name, sm, md, lg]) => ({name: capitalize(name),
							 sm, md, lg}));

// import statements
const importStatements = `import React from 'react';
import styled from 'styled-components';
import clsx from 'clsx';
`;
// generate styled functions definitions
const styledfns = objLayouts.map(({name, sm, md, lg}) => 
                                 `const ${name}Base = styled('div').attrs(props => ({
    className: clsx("fl pa2 w-${mapSize(sm)} w-${mapSize(md)}-m w-${mapSize(lg)}-l", 
               props.all && "w-20")
}))\`\`;`
                                );

// generates the layout component function
const fns = objLayouts.map(({name, sm, md, lg}) =>
                           `function ${name}({children}){
    return (
	<${name}Base>
	    {children}
	</${name}Base>
    );
}
export default ${name};
`);

const exportStatements = objLayouts.map(({name, sm, md, lg}) =>
                                        `export default ${name}`
                                       );

const zip = (a, b) => a.map((item, index) => [item, b[index]]);

const fnsAndStyledfns = zip(fns, styledfns).map(x => x.join('\n'));
const layoutFiles = fnsAndStyledfns.map(x => importStatements.concat(x));
const layoutAndObjLayouts = zip(layoutFiles, objLayouts);
const componentsPath = path.resolve(__dirname, '../src/components/');

// scripts expects the paths to already exist
layoutAndObjLayouts.forEach(([layoutFile, layoutObj]) => {
    const componentPath = path.resolve(componentsPath, layoutObj.name);
    const componentFilePath = path.resolve(componentPath, "index.js");
    fs.writeFileSync(componentFilePath, layoutFile);
});

module.exports = {
    layouts,
    capitalize,
    zip
};
