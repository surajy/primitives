const fs = require('fs');
const path = require('path');
// all generated layouts
const { layouts, capitalize, zip } = require('./layouts.js');

const componentsPath = path.resolve(__dirname, "../src/components");
const layoutNames = layouts.map(layout => capitalize(layout[0]));
const layoutSizes = layouts.map(layout => layout.pop());
const importStatements = layoutNames.map(layout => {
    const importFromPath = path.relative(path.resolve(componentsPath, "Layout"),
                                         path.resolve(componentsPath, layout));
    const importStatement = `import ${layout} from '${importFromPath}';`;
    return importStatement;
}).concat([`import React from 'react';`]);

const casesOnly = zip(layoutNames, layoutSizes)
      .map(([name, size]) => [`case ${size}:`, `return <${name}>{children}</${name}>;`]);


const casesWithDefault = casesOnly.concat([[`default:`, `return Single;`]]);

const casesToString = casesWithDefault.map(x => x.join('\n')).join('\n');

const exportStatement = 'export default Layout;';

const layoutfn = `
function Layout({size, children}){
switch(size){
${casesToString}
}
}
`;

const fileContents = [importStatements.join('\n'), layoutfn, exportStatement].join('\n');

const layoutFilePath = path.resolve(componentsPath, "Layout", "index.js");
fs.writeFileSync(layoutFilePath, fileContents);
