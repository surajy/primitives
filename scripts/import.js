const fs = require('fs');
const path = require('path');

const componentsPath = path.resolve(__dirname, "../","src", "components"); // set components dir
const all = fs.readdirSync(componentsPath, (err, components) => components);
const components = all.filter(x => x !== 'index.js');

// change this only
const files = [
    {name: "index.js", prefix: "./src/components", postfix: "Base", path: "./"},
    {name: "index.js", prefix: ".", postfix: "Base", path: "./src/components/"}
];

const importStatement = (component, pre, post) => `import ${component}${post} from '${pre}/${component}';`;
const exportStatement = component => `export const ${component} = ${component}Base;`;

const filesImportContents =
    files.map(file => 
        components
            .map(component => importStatement(component, file.prefix, file.postfix))
    );

const componentsString = 
    components.map(component => `${component}, `)
	      .join('\n');

const exportContents = components.map(component => exportStatement(component));

const filesExportContents = exportContents.join('\n');

files.forEach((file, index) => {
    const filePath = path.resolve(file.path, file.name);
    const fileContent = filesImportContents[index].join('\n') + '\n' + filesExportContents;
    fs.writeFileSync(filePath, fileContent);
});


