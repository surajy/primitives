const fs = require("fs");
const path = require("path");
const { exec } = require("child_process");

const componentsPath = path.resolve(__dirname, "../", "src", "components");
const componentFilePath = path.resolve(__dirname, "../src/components/Component/index.js");
// import path in file
const importPath = "./index.js";
const all = fs.readdirSync(componentsPath, (err, components) => components);
const components = all.filter(x => x !== 'index.js').filter(x => x !== "Component");
const allProps = "allProps"; // combined props in one

const componentsImports = components
      .map(component => `import ${component} from '../${component}';`)
      .join('\n');

// import statements
const importsString =
      `import React from 'react';\n
${componentsImports};
`;

const exportString = 
      `export default function({type, subtype, data, extra}){
    const size = data?.layout || extra?.layout || 1;
    return (
        <Layout size={size}>
          <Component type={type} subtype={subtype} data={data} extra={extra}/>
        </Layout>
    );
}`;


// no default case
const onlyCases = components
      .map(component => [`case "${component.toLowerCase()}":`,
                         `return (<${component} {...${allProps}}/>);`].join('\n'))
      .join('\n');

// includes default case
const allCases = [onlyCases, `default:\nreturn null;`].join('\n');

// wrap function body around cases
const functionWrapper = cases => 
      `function Component({type, subtype, data, extra}){
const allProps = {
        type: subtype,
        ...data,
        ...extra
    };

if (type === "element")
        return <Element children={data} {...extra}/>;

switch(type){
${cases}
}
}
`;

const functionCode = functionWrapper(allCases);

const code = [importsString, functionCode, exportString].join('\n');

fs.writeFileSync(componentFilePath, code);
